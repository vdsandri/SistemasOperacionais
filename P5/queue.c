

/*
Equipe:

    Alan de farias da Silva  e Vinícius Sandri Diaz



    Primeiro trabalho de S0


*/
#include "queue.h"

//------------------------------------------------------------------------------
// Insere um elemento no final da fila.
// Condicoes a verificar, gerando msgs de erro:
// - a fila deve existir
// - o elemento deve existir
// - o elemento nao deve estar em outra fila

void queue_append (queue_t **queue, queue_t *elem)
{


    queue_t *aux;
    aux = *queue;   

    if(queue == NULL)
    {
        puts("ERROR! queue inexistente\n");
       //return ;

    }

    else if(elem == NULL)
    {

        puts("ERROR! elemento inexistente\n");
     // return ;

    }

    //Verifica se o elemento já existe na Fila ou pertence a outra fila
    else if(elem->next != NULL && elem->prev != NULL)
    {
        int c = queue_size(*queue);

        do{
            if(elem == aux)
            {
                puts("ERROR! O elemento jah existe na fila\n");
            }
            aux = aux->next;//Para percorrer a fila
            c--;
        }while (c>0);

        
        puts("ERROR! O elemento jah existe em outra fila\n");
  //    

    }

    else if(!elem->next && !elem->prev)
    {
        int tam = queue_size(*queue);//Usando a função para ver o tamanho da queue.


		if (tam == 0)//Se a queue for vazia
		{
			elem->next = elem;
			elem->prev = elem;
			*queue = elem;
		}

		else
		{
			
			elem->next = *queue;
			elem->prev = (*queue)->prev;
			(*queue)->prev->next = elem;
			(*queue)->prev = elem;
		}


    }

    else
    {
        puts("ERROR! Elemento invalido. Ponteiros do elemento incorretos.\n");

    }



}

//------------------------------------------------------------------------------
// Remove o elemento indicado da fila, sem o destruir.
// Condicoes a verificar, gerando msgs de erro:
// - a fila deve existir
// - a fila nao deve estar vazia
// - o elemento deve existir
// - o elemento deve pertencer a fila indicada
// Retorno: apontador para o elemento removido, ou NULL se erro

queue_t *queue_remove (queue_t **queue, queue_t *elem)

{


    queue_t *aux;
    aux = *queue;
    bool elementoFila = false;
    int tam;
    tam = queue_size(*queue);

   
    do{
        if (elem == aux) {
            elementoFila = true;
        }
        aux = aux->next;
    }while (aux != *queue && !elementoFila);


    if(!queue){
        puts("ERROR para remover! A fila nao existe.\n");
		return NULL;
    }

    else if (tam == 0){
        puts("ERROR para remover! A fila esta vazia.\n");
        return NULL;
    }

    else if (!elem){
        puts("ERROR para remover! O elemento nao existe.\n");
        return NULL;
    }


    else if (elementoFila == false && elem->next != NULL && elem->prev != NULL){
    		puts("ERROR para remover! O elemento nao pertence a esta fila.\n");
    		return NULL;
    	}

    else if (elem->next == NULL && elem->prev == NULL){
		puts("ERROR para remover! O elemento a ser removido nao pertence a nenhuma fila.\n");
		return NULL;
	}

    else if(elementoFila)
    {


        if (elem == *queue)	//se o elemento a ser removido for o "primeiro" da fila
        {
          if (tam == 1)
          {
            *queue = NULL;
            elem->next = NULL;
            elem->prev = NULL;
          }

          else
          {
            *queue = elem->next;
            (*queue)->prev = elem->prev;
            (*queue)->prev->next = elem->next;
            elem->next = NULL;
            elem->prev = NULL;
          }
        }

        else
        {
            aux = elem->prev;
            aux->next = elem->next;
            aux = elem->next;
            aux->prev = elem->prev;
            elem->next = NULL;
            elem->prev = NULL;
        }


    }

    return elem;

}

//------------------------------------------------------------------------------
// Conta o numero de elementos na fila
// Retorno: numero de elementos na fila

int queue_size (queue_t *queue)

{
    queue_t *aux;
    aux = queue;
    int tam = 0;

    if(queue == NULL)
    {
        //puts("Fila Vazia\n");
        return 0;
    }

    else
    {

        do
        {
            aux = aux->next;
            tam++;

        }while (aux!= queue);

        return tam;

    }

}

//------------------------------------------------------------------------------
// Percorre a fila e imprime na tela seu conteúdo. A impressão de cada
// elemento é feita por uma função externa, definida pelo programa que
// usa a biblioteca.
//
// Essa função deve ter o seguinte protótipo:
//
// void print_elem (void *ptr) ; // ptr aponta para o elemento a imprimir

void queue_print (char *name, queue_t *queue, void print_elem (void*) )
{
  printf("%s:", name);

  	queue_t *aux;
  	aux = queue;

  	if(!queue)
  	{
  		print_elem(aux);
  	}

  	else
  	{
  		do{
  			print_elem(aux);
  			aux = aux->next;

  		}while(aux != queue);
  	}
  }
  
 
