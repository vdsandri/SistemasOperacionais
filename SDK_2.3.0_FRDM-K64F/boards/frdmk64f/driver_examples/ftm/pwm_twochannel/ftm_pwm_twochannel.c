	/*
	 * Copyright (c) 2015, Freescale Semiconductor, Inc.
	 * Copyright 2016-2017 NXP
	 *
	 * Redistribution and use in source and binary forms, with or without modification,
	 * are permitted provided that the following conditions are met:
	 *
	 * o Redistributions of source code must retain the above copyright notice, this list
	 *   of conditions and the following disclaimer.
	 *
	 * o Redistributions in binary form must reproduce the above copyright notice, this
	 *   list of conditions and the following disclaimer in the documentation and/or
	 *   other materials provided with the distribution.
	 *
	 * o Neither the name of the copyright holder nor the names of its
	 *   contributors may be used to endorse or promote products derived from this
	 *   software without specific prior written permission.
	 *
	 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
	 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
	 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
	 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
	 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
	 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
	 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
	 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
	 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
	 */

	#include "fsl_debug_console.h"
	#include "board.h"
	#include "fsl_ftm.h"

	#include "pin_mux.h"
	#include "clock_config.h"
	/*******************************************************************************
	 * Definitions
	 ******************************************************************************/
	/* The Flextimer instance/channel used for board */
	#define BOARD_FTM_BASEADDR FTM0
	#define BOARD_FIRST_FTM_CHANNEL 0U
	#define BOARD_SECOND_FTM_CHANNEL 1U
	#define BOARD_THIRD_FTM_CHANNEL 2U

	/* Get source clock for FTM driver */
	#define FTM_SOURCE_CLOCK CLOCK_GetFreq(kCLOCK_BusClk)

	/*******************************************************************************
	 * Prototypes
	 ******************************************************************************/
	/*!
	 * @brief delay a while.
	 */
	void delay(void);
	void iniciaPWM(void);
	void aceleraPWM1(void);
	void aceleraPWM2(void);
	void reduzPWM1(void);
	void reduzPWM2(void);

	/*******************************************************************************
	 * Variables
	 ******************************************************************************/
	 uint8_t PWM1 = 0U;
	 uint8_t PWM2 = 0U;
	/*******************************************************************************
	 * Code
	 ******************************************************************************/
	void delay(void)
	{
			volatile uint32_t i = 0U;
			for (i = 0U; i < 800000U; ++i)
			{
					__asm("NOP"); /* delay */
			}
	}

	void iniciaPWM(void)

	{
			ftm_config_t ftmInfo;
			ftm_chnl_pwm_signal_param_t ftmParam[2];

			/* Configure ftm params with frequency 24kHZ */
			ftmParam[0].chnlNumber = (ftm_chnl_t)BOARD_SECOND_FTM_CHANNEL;
			ftmParam[0].level = kFTM_LowTrue;
			ftmParam[0].dutyCyclePercent = 0U;
			ftmParam[0].firstEdgeDelayPercent = 0U;

			ftmParam[1].chnlNumber = (ftm_chnl_t)BOARD_THIRD_FTM_CHANNEL;
			ftmParam[1].level = kFTM_LowTrue;
			ftmParam[1].dutyCyclePercent = 0U;
			ftmParam[1].firstEdgeDelayPercent = 0U;

			/* Board pin, clock, debug console init */
			BOARD_InitPins();
			BOARD_BootClockRUN();
			BOARD_InitDebugConsole();

			/* Print a note to terminal */
			/*
			PRINTF("\r\nFTM example to output PWM on 2 channels\r\n");
			PRINTF("\r\nYou will see a change in LED brightness if an LED is connected to the FTM pin");
			PRINTF("\r\nIf no LED is connected to the FTM pin, then probe the signal using an oscilloscope");
			*/

			FTM_GetDefaultConfig(&ftmInfo);
			/* Initialize FTM module */
			FTM_Init(BOARD_FTM_BASEADDR, &ftmInfo);
			FTM_SetupPwm(BOARD_FTM_BASEADDR, ftmParam, 2U, kFTM_EdgeAlignedPwm, 24000U, FTM_SOURCE_CLOCK);
			FTM_StartTimer(BOARD_FTM_BASEADDR, kFTM_SystemClock);
		
	}	

	
	
void updatePWM(int PWM, uint8_t dc){
	if (PWM == 1){
		FTM_UpdatePwmDutycycle(BOARD_FTM_BASEADDR, (ftm_chnl_t)BOARD_SECOND_FTM_CHANNEL, kFTM_EdgeAlignedPwm,
                               dc);//PTC2
	}
	else{
		FTM_UpdatePwmDutycycle(BOARD_FTM_BASEADDR, (ftm_chnl_t)BOARD_THIRD_FTM_CHANNEL, kFTM_EdgeAlignedPwm,
                               dc);//PTC2
	}
		        
        /* Software trigger to update registers */
  FTM_SetSoftwareTrigger(BOARD_FTM_BASEADDR, true);
}


	 void aceleraPWM1(void)
	 {
		 if (PWM1 >=10U)
		 {
				PWM1 -= 10U;
		 }
		 
		 else
		 {
			 PWM1 = 0U;
		 }
		 
		 updatePWM(1, PWM1);

	 }
	 
		void aceleraPWM2(void)
	 {
		 if (PWM2 >=10U)
		 {
				PWM2 -= 10U;
		 }
		 
		 else
		 {
			 PWM2 = 0U;
		 }
		 
		 updatePWM(2, PWM2);

	 }

		void reduzPWM1(void)
	 {
		 if (PWM1 <=90U)
		 {
				PWM1 += 10U;
		 }
		 
		 else
		 {
			 PWM1 = 100U;
		 }
		 
	updatePWM(1, PWM1);
	 }
	 
	 
		void reduzPWM2(void)
	 {
		 if (PWM2 <=90U)
		 {
				PWM2 += 10U;
		 }
		 
		 else
		 {
			 PWM2 = 100U;
		 }
		 
	updatePWM(2, PWM2);
	 }
	 
	 
	 
	/*!
	 * @brief Main function
	 */
//	int main(void)
//	{
//		uint8_t i = 0U;
//		 iniciaPWM(); 
//		 updatePWM(1, 100U);
//	 	 updatePWM(2, 0U);
//			while (1)
//			{
////				if (i<100U)
////					i+=10U;
////				
////				else
////					i=0U;
////				
////				delay();
////				updatePWM(1, i);
////				updatePWM(2, 100U-i);
//				
//				aceleraPWM1() ;
//				reduzPWM2();
//				if (PWM1 == 0U)
//					PWM1 = 100U;
//				if (PWM2 == 100U)
//					PWM2 = 0U;
//				delay();

//			}
//	}

///*
// * Copyright (c) 2015, Freescale Semiconductor, Inc.
// * Copyright 2016-2017 NXP
// *
// * Redistribution and use in source and binary forms, with or without modification,
// * are permitted provided that the following conditions are met:
// *
// * o Redistributions of source code must retain the above copyright notice, this list
// *   of conditions and the following disclaimer.
// *
// * o Redistributions in binary form must reproduce the above copyright notice, this
// *   list of conditions and the following disclaimer in the documentation and/or
// *   other materials provided with the distribution.
// *
// * o Neither the name of the copyright holder nor the names of its
// *   contributors may be used to endorse or promote products derived from this
// *   software without specific prior written permission.
// *
// * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
// * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
// * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// */

//#include "fsl_debug_console.h"
//#include "board.h"
//#include "fsl_ftm.h"

//#include "pin_mux.h"
//#include "clock_config.h"
///*******************************************************************************
// * Definitions
// ******************************************************************************/
///* The Flextimer instance/channel used for board */
//#define BOARD_FTM_BASEADDR FTM0
//#define BOARD_FIRST_FTM_CHANNEL 0U
//#define BOARD_SECOND_FTM_CHANNEL 1U
//#define BOARD_THIRD_FTM_CHANNEL 2U

///* Get source clock for FTM driver */
//#define FTM_SOURCE_CLOCK CLOCK_GetFreq(kCLOCK_BusClk)
//uint8_t PWM1 = 0U;
//uint8_t PWM2 = 0U;
///*******************************************************************************
// * Prototypes
// ******************************************************************************/
///*!
// * @brief delay a while.
// */
//void delay(void);

///*******************************************************************************
// * Variables
// ******************************************************************************/

///*******************************************************************************
// * Code
// ******************************************************************************/
//void delay(void)
//{
//    volatile uint32_t i = 0U;
//    for (i = 0U; i < 800000U; ++i)
//    {
//        __asm("NOP"); /* delay */
//    }
//}

//	void iniciaPWM(void)

//	{
//			ftm_config_t ftmInfo;
//			ftm_chnl_pwm_signal_param_t ftmParam[2];

//			/* Configure ftm params with frequency 24kHZ */
//			ftmParam[0].chnlNumber = (ftm_chnl_t)BOARD_SECOND_FTM_CHANNEL;
//			ftmParam[0].level = kFTM_LowTrue;
//			ftmParam[0].dutyCyclePercent = 0U;
//			ftmParam[0].firstEdgeDelayPercent = 0U;

//			ftmParam[1].chnlNumber = (ftm_chnl_t)BOARD_THIRD_FTM_CHANNEL;
//			ftmParam[1].level = kFTM_LowTrue;
//			ftmParam[1].dutyCyclePercent = 0U;
//			ftmParam[1].firstEdgeDelayPercent = 0U;

//			/* Board pin, clock, debug console init */
//			BOARD_InitPins();
//			BOARD_BootClockRUN();
//			BOARD_InitDebugConsole();

//			/* Print a note to terminal */
//			/*
//			PRINTF("\r\nFTM example to output PWM on 2 channels\r\n");
//			PRINTF("\r\nYou will see a change in LED brightness if an LED is connected to the FTM pin");
//			PRINTF("\r\nIf no LED is connected to the FTM pin, then probe the signal using an oscilloscope");
//			*/

//			FTM_GetDefaultConfig(&ftmInfo);
//			/* Initialize FTM module */
//			FTM_Init(BOARD_FTM_BASEADDR, &ftmInfo);
//			FTM_SetupPwm(BOARD_FTM_BASEADDR, ftmParam, 2U, kFTM_EdgeAlignedPwm, 24000U, FTM_SOURCE_CLOCK);
//			FTM_StartTimer(BOARD_FTM_BASEADDR, kFTM_SystemClock);
//		
//	}	


///*!
// * @brief Main function
// */

//void updatePWM(int PWM, uint8_t dc){
//	if (PWM == 1){
//		FTM_UpdatePwmDutycycle(BOARD_FTM_BASEADDR, (ftm_chnl_t)BOARD_SECOND_FTM_CHANNEL, kFTM_EdgeAlignedPwm,
//                               dc);//PTC2
//	}
//	else{
//		FTM_UpdatePwmDutycycle(BOARD_FTM_BASEADDR, (ftm_chnl_t)BOARD_THIRD_FTM_CHANNEL, kFTM_EdgeAlignedPwm,
//                               dc);//PTC2
//	}
//		        
//        /* Software trigger to update registers */
//  FTM_SetSoftwareTrigger(BOARD_FTM_BASEADDR, true);
//}

//		void reduzPWM2(void)
//	 {
//		 if (PWM2 <90U)
//		 {
//				PWM2 += 10U;
//		 }
//		 
//		 else
//		 {
//			 PWM2 = 100U;
//		 }
//		 
//		updatePWM(2, PWM2);

//	 }
//int main(void)
//{
//	iniciaPWM();
//	updatePWM(1, 10U);
//	updatePWM(2, 10U);
//    while (1)
//    {
//        /* Delay to see the change of LEDs brightness */
//        delay();
//				reduzPWM2();
//        
//        /* Start PWM mode with updated duty cycle */
//				/* Essa fun��o implementa o PWM em porcentagem  pinos PTC2 e PTC3*/

//    }
//}



