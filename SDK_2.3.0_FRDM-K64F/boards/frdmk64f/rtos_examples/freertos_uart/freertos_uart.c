/*
 * Copyright (c) 2015, Freescale Semiconductor, Inc.
 * Copyright 2016-2017 NXP
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * o Redistributions of source code must retain the above copyright notice, this list
 *   of conditions and the following disclaimer.
 *
 * o Redistributions in binary form must reproduce the above copyright notice, this
 *   list of conditions and the following disclaimer in the documentation and/or
 *   other materials provided with the distribution.
 *
 * o Neither the name of the copyright holder nor the names of its
 *   contributors may be used to endorse or promote products derived from this
 *   software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/* FreeRTOS kernel includes. */
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "timers.h"

/* Freescale includes. */
#include "fsl_device_registers.h"
#include "fsl_debug_console.h"
#include "board.h"

#include "fsl_uart_freertos.h"
#include "fsl_uart.h"

#include "pin_mux.h"
#include "clock_config.h"
/*******************************************************************************
 * Definitions
 ******************************************************************************/
#include "FreeRTOSConfig.h"
/* UART instance and clock */
#define DEMO_UART UART4
#define DEMO_UART_CLKSRC UART4_CLK_SRC
#define DEMO_UART_CLK_FREQ CLOCK_GetFreq(UART4_CLK_SRC)
#define DEMO_UART_RX_TX_IRQn UART4_RX_TX_IRQn
/* Task priorities. */
#define uart_task_PRIORITY (configMAX_PRIORITIES - 1)
/*******************************************************************************
 * Prototypes
 ******************************************************************************/
static void uart_task(void *pvParameters);

/*******************************************************************************
 * Variables
 ******************************************************************************/

const char *send_ring_overrun = "\r\nRing buffer overrun!\r\n";
const char *send_hardware_overrun = "\r\nHardware buffer overrun!\r\n";
const char *cmd0 = "AT\r\n";
const char *cmd1 = "AT+GMR\r\n";
const char *cmd2 = "AT+RST\r\n";
const char *cmd3 = "AT+CWMODE=3\r\n";
const char *cmd4 = "AT+CWLAP\r\n";

uint8_t background_buffer[32];
uint8_t recv_buffer[1];

uart_rtos_handle_t handle;
struct _uart_handle t_handle;

uart_rtos_config_t uart_config = {
    .baudrate = 115200,
    .parity = kUART_ParityDisabled,
    .stopbits = kUART_OneStopBit,
    .buffer = background_buffer,
    .buffer_size = sizeof(background_buffer),
};



/*******************************************************************************
 * Code
 ******************************************************************************/
/*!
 * @brief Application entry point.
 */
int main(void)
{
    /* Init board hardware. */
    BOARD_InitPins();
		BOARD_InitDEBUG_UART();
    BOARD_BootClockRUN();
		BOARD_InitDebugConsole();
    NVIC_SetPriority(DEMO_UART_RX_TX_IRQn, 5);
		PRINTF("\nTST\r\n");
    xTaskCreate(uart_task, "Uart_task", configMINIMAL_STACK_SIZE + 10, NULL, uart_task_PRIORITY, NULL);

    vTaskStartScheduler();
    for (;;)
        ;
}

/*!
 * @brief Task responsible for printing of "Hello world." message.
 */
static void uart_task(void *pvParameters)
{
    int error;
    size_t n;
		char * recv = "";
    uart_config.srcclk = DEMO_UART_CLK_FREQ;
    uart_config.base = DEMO_UART;
    if (0 > UART_RTOS_Init(&handle, &t_handle, &uart_config))
    {
				PRINTF("Init Error\r\n");
        vTaskSuspend(NULL);
    }
		
    /* Send some data */
    if (0 > UART_RTOS_Send(&handle, (uint8_t *)cmd0, strlen(cmd0)))
    {
				PRINTF("Send Error\r\n");
        vTaskSuspend(NULL);
    }
		
    /* Send data */
    do
    {
				PRINTF("TST");

        error = UART_RTOS_Receive(&handle, recv_buffer, sizeof(recv_buffer), &n);
				PRINTF("TST2");

				PRINTF ("%c", recv_buffer[0]);
 


				
        vTaskDelay(10);
				
    } while (kStatus_Success == error);
		
		UART_RTOS_Deinit(&handle);
		
		PRINTF ("end");

    vTaskSuspend(NULL);
}
