#include "PWM.h"

/*******************************************************************************
	 * Variables
	 ******************************************************************************/
	 uint8_t PWM1 = 0U;
	 uint8_t PWM2 = 0U;

void delay(void)
{
    volatile uint32_t i = 0U;
    for (i = 0U; i < 800000U; ++i)
    {
        __asm("NOP"); /* delay */
    }
}

uint8_t getPWM1(void){
	return PWM1;
}


uint8_t getPWM2(void){
	return PWM2;
}

void iniciaPWM(void)

{
    ftm_config_t ftmInfo;
    ftm_chnl_pwm_signal_param_t ftmParam[2];
    
    /* Configure ftm params with frequency 24kHZ */
    ftmParam[0].chnlNumber = (ftm_chnl_t)PWM_CHANNEL1;
    ftmParam[0].level = kFTM_LowTrue;
    ftmParam[0].dutyCyclePercent = 0U;
    ftmParam[0].firstEdgeDelayPercent = 0U;
    
    ftmParam[1].chnlNumber = (ftm_chnl_t)PWM_CHANNEL2;
    ftmParam[1].level = kFTM_LowTrue;
    ftmParam[1].dutyCyclePercent = 0U;
    ftmParam[1].firstEdgeDelayPercent = 0U;
    
    /* Board pin, clock, debug console init */
    BOARD_InitPins();
    BOARD_BootClockRUN();
    BOARD_InitDebugConsole();
    
        
    FTM_GetDefaultConfig(&ftmInfo);
    /* Initialize FTM module */
    FTM_Init(BOARD_FTM_BASEADDR, &ftmInfo);
    FTM_SetupPwm(BOARD_FTM_BASEADDR, ftmParam, 2U, kFTM_EdgeAlignedPwm, 24000U, FTM_SOURCE_CLOCK);
    FTM_StartTimer(BOARD_FTM_BASEADDR, kFTM_SystemClock);
    
}	



void updatePWM(int PWM, uint8_t dc){
    if (PWM == 1){
        FTM_UpdatePwmDutycycle(BOARD_FTM_BASEADDR, (ftm_chnl_t)PWM_CHANNEL1, kFTM_EdgeAlignedPwm,
                               dc);//PTC2
    }
    else{
        FTM_UpdatePwmDutycycle(BOARD_FTM_BASEADDR, (ftm_chnl_t)PWM_CHANNEL2, kFTM_EdgeAlignedPwm,
                               dc);//PTC2
    }
    
    /* Software trigger to update registers */
    FTM_SetSoftwareTrigger(BOARD_FTM_BASEADDR, true);
}


void aceleraPWM1(void)
{
    if (PWM1 >=10U)
    {
        PWM1 -= 10U;
    }
    
    else
    {
        PWM1 = 0U;
    }
    
    updatePWM(1, PWM1);
    
}

void aceleraPWM2(void)
{
    if (PWM2 >=10U)
    {
        PWM2 -= 10U;
    }
    
    else
    {
        PWM2 = 0U;
    }
    
    updatePWM(2, PWM2);
    
}

void reduzPWM1(void)
{
    if (PWM1 <=90U)
    {
        PWM1 += 10U;
    }
    
    else
    {
        PWM1 = 100U;
    }
    
    updatePWM(1, PWM1);
}


void reduzPWM2(void)
{
    if (PWM2 <=90U)
    {
        PWM2 += 10U;
    }
    
    else
    {
        PWM2 = 100U;
    }
    
    updatePWM(2, PWM2);
}

void loopPWM(void){

				aceleraPWM1() ;
				reduzPWM2();
				if (PWM1 == 0U)
					PWM1 = 100U;
				if (PWM2 == 100U)
					PWM2 = 0U;
				delay();

}

void esquerdaFrente(void)
{
    PWM1 = 15U;
    PWM2 = 90U;
}
void direitaFrente(void)
{
    PWM1 = 90U;
    PWM2 = 15U;
}
void esquerdaAtras(void)
{
    GPIO_WritePinOutput(BOARD_M1_GPIO, BOARD_M1_PIN, 1U);
    GPIO_WritePinOutput(BOARD_M2_GPIO, BOARD_M2_PIN, 1U);
    PWM1 = 100U;
    PWM2 = 0U;
    
}
void direitaAtras(void)
{
    
    GPIO_WritePinOutput(BOARD_M1_GPIO, BOARD_M1_PIN, 1U);
    GPIO_WritePinOutput(BOARD_M2_GPIO, BOARD_M2_PIN, 1U);
    PWM1= 0U;
    PWM2 = 100U;
}

void motorFrente(void)
{
    PWM1 = 15U;
    PWM2 = 15U;
}
void motorAtras(void)
{
    GPIO_WritePinOutput(BOARD_M1_GPIO, BOARD_M1_PIN, 1U);
    GPIO_WritePinOutput(BOARD_M2_GPIO, BOARD_M2_PIN, 1U);
    PWM1 = 0U;
    PWM2 = 0U;
}

void aceleraMotores(void)
{
    aceleraPWM1();
    aceleraPWM2() ;
}
void reduzMotores(void)
{
    
    reduzPWM1();
    reduzPWM2();
}

void rotacionaEsquerda(void)
{
    reduzPWM1();
    aceleraPWM2();
}
void rotacionaDireita(void)
{
    aceleraPWM1();
    reduzPWM2();
}


void paraMotor(void)
{
    
    PWM1 = 100U;
    PWM2 = 100U;
    GPIO_WritePinOutput(BOARD_M1_GPIO, BOARD_M1_PIN, 0U);
    GPIO_WritePinOutput(BOARD_M2_GPIO, BOARD_M2_PIN, 0U);
}    


void control(char d)
{
    updatePWM(1, 100U);
    updatePWM(2, 100U);
    
    

        if(d == 'w')
        {
						PRINTF("FRENTE\n");
            motorFrente();
        }
        else if(d == 's')
        {
            motorAtras();
        }
        
        else if(d == 'q')
        {
            esquerdaFrente();
            
        }
        else if(d == 'e')
        {
            direitaFrente();
            
        }
        else if(d == 'z')
        {
            esquerdaAtras();
            
        }
        else if(d == 'c')
        {
            direitaAtras();
        }   
        
        else if(d == 'g')
        {
            aceleraMotores();
            
        }
        
        else if(d == 'h')
        {
            reduzMotores();
            
        }
        
        else if(d == 'a')
        {
            rotacionaEsquerda();
            
        }
        else if(d == 'd')
        {
            rotacionaDireita();
            
        }
        else if(d == 'p')
        {
            paraMotor();
            
        }
    
}




