/*
 * Ping Pong OS
 * preempcao.c
 * Projeto da disciplina Sistemas Operacionais - UTFPR - 2017.02
 * Professor: Copetti
 * Alunos: Alan de Farias da Silva
 *         Vinícius Sandri Diaz
 *
 * Data da Ultima modificação: 29/10 - 18:24 by Alan
 *
 * pra compilar com o DEBUG: gcc -o preempcao -W -DDEBUG  pingpong-preempcao.c ppos.c queue.c
 * 
 *              SEM o DEBUG: gcc -o preempcao -W  pingpong-preempcao.c ppos.c queue.c
 */


#include "ppos.h"

#define STACKSIZE 32768
#define TIMEQUANTUM 20

task_t* taskAtual = NULL;
task_t* taskQueue = NULL;


task_t task_main;
task_t dispatcher;

int id = 0;
int quantum = 0;
unsigned int time = 0;


// estrutura de inicialização to timer
struct itimerval timer;

// estrutura que define um tratador de sinal (deve ser global ou static)
struct sigaction action ;

//Funções Contidas apenas no ppos.c

/*Alan, essa função foi implementada no fim do arquivo*/
void tratador(int signum);

task_t* scheduler()
{
    task_t* next;
    task_t* iterator;
    int bestPrio = task_getprio(taskQueue) - task_getprioDin(taskQueue);
    iterator = taskQueue;
    next = iterator;
    do
    {
        if ((task_getprio(iterator) - task_getprioDin(iterator)) < bestPrio)
        {
            
            bestPrio = (task_getprio(iterator) - task_getprioDin(iterator));
            next = iterator;
            
        }
        
        iterator = iterator->next;
        
    }while (iterator!= taskQueue);
    
    task_setprioDin(next, 0);
    iterator = taskQueue;
    do
    {
        task_setprioDin(iterator, task_getprioDin(iterator) + 1);
        iterator = iterator->next;
        
    }while (iterator!= taskQueue);
    
    #ifdef DEBUG
    printf ("Scheduler: proximo processo é %d, com prioridade %d\n", iterator->tid, task_getprio(iterator) ) ;
    #endif
    
    return next;
    
    
}


//Essa função não existe na ppos.h
void dispatcher_body (void* arg)
{
    
    /* Alan essa função foi desenvolvida de acordo com a página do Maziero:
     * http://wiki.inf.ufpr.br/maziero/doku.php?id=so:dispatcher
     *
     * Praticamento é a mesma função descrita, só instanciei o ponteiro e
     * usei a queue para percorrer as tasks
     */
    task_t* next;
    next = NULL;
    // se o numero de tarefas for maior que zero
    while (queue_size ((queue_t *) taskQueue) > 0)
    {
        // ... // ações antes de lançar a tarefa "next", se houverem
	
        next = scheduler(); // scheduler é uma função
        
        //... // ações após retornar da tarefa "next", se houverem
        if(next)
        {
            next->quantum = TIMEQUANTUM ;
            queue_remove((queue_t **) &taskQueue, (queue_t *) next);
	    
            task_switch(next);// transfere controle para a tarefa "next"
             
            
        }
        
        
    }
    
    task_exit(0); // encerra a tarefa dispatcher
}


void task_yield ()
{	
    /*Se a tarefa chamou yield, devolve o processador ao dispatcher e retorna ao fim da fila de tarefas prontas
     */
    if (taskAtual->tid > 0)
        queue_append ((queue_t **) &taskQueue, (queue_t *) taskAtual);
    taskAtual->proc_time += systime() - taskAtual->last_time;
    task_switch(&dispatcher);
}

void ppos_init ()
{
    /*
     * Alan pensando no que conversamos ontem achei uma forma de
     * inicializar a task da main. Eu acho que do jeito que esta
     * não fica aquela coisa tipo do nada, sem contexto. E desta forma, a task_main é incializada
     * No entanto, não sei se vai ficar legal na pilha. Só foi uma idéia, se achar ela meio bosta
     * fique à vontade pra modificar
     *
     */
    
    taskQueue = NULL;
    task_main.tid = id; //Sendo que o id = 0
    
    /*
     * Inicializa a task de acordo com o site
     * https://www.systutorials.com/docs/linux/man/3-getcontext/
     * */
    
    getcontext(&task_main.context);
    taskAtual = &task_main;
    
    task_create(&dispatcher, dispatcher_body, "dispatcher");
    task_setprio(&dispatcher, -20);
    
    /*
     * desativa o buffer da saida padrao (stdout), usado pela função printf
     *
     * Essa função esta na página do mazeiro e ele recomeda desativar no inicio
     * por isso coloquei aqui. Qualquer coisa Alan, veja aqui:
     * http://wiki.inf.ufpr.br/maziero/doku.php?id=so:gestao_de_tarefas
     */
    setvbuf (stdout, 0, _IONBF, 0) ;
    
    #ifdef DEBUG
    printf ("task_init: inicializou a tarefa %d\n", taskAtual->tid) ;
    #endif
    //coloquei esse print de acordo com a recomedação do Mazieiro
    
    // registra a aï¿œï¿œo para o sinal SIGINT
    action.sa_handler = tratador;
    sigemptyset (&action.sa_mask) ;
    action.sa_flags = 0 ;
    
    if (sigaction (SIGALRM, &action, 0) < 0)
    {
        perror ("Erro em sigaction: ") ;
        exit (1) ;
    }
    
    /* Alan, aqui está de acordo com o Maziero: http://wiki.inf.ufpr.br/maziero/doku.php?id=so:preempcao_por_tempo
     *  
     * Durante a inicialização do sistema, um temporizador deve ser programado para disparar a cada 1
     * milissegundo; 
     * 
     */
    
    // ajusta valores do temporizador
    timer.it_value.tv_usec = 100;        // primeiro disparo, em micro-segundos
    timer.it_value.tv_sec  = 0;         // primeiro disparo, em segundos
    timer.it_interval.tv_usec = 1000;   // disparos subsequentes, em micro-segundos
    timer.it_interval.tv_sec  = 0;      // disparos subsequentes, em segundos
    
    
    // arma o temporizador ITIMER_REAL (vide man setitimer)
    if (setitimer (ITIMER_REAL, &timer, 0) < 0)
    {
        perror ("Erro em setitimer: ") ;
        exit (1) ;
    }
    
    id++;

    
}
int task_create (task_t * task, void (*start_routine)(void *),  void * arg)
{
    /*Alan, como vc pode ver no enunciado abaixo da página do Mazieiro,
     * adaptei a main da context.c nesta função. Vc pode conferir a dica abaixo
     */
    
    /* Atenção: deve ser previsto um descritor de tarefa que aponte para o programa principal
     *    ( que exercerá a mesma função da variável ContextMain no programa pingpong.c)
     */
    
    /* task: estrutura que referencia a tarefa criada
     *      start_routine: função que será executada pela tarefa
     *      arg: parâmetro a passar para a tarefa que está sendo criada
     *      retorno: o ID da task ou valor negativo, se houver erro
     */
    
    char* stack;
    task_t *aux;
     
    
    getcontext(&task->context);
    
    stack = malloc (STACKSIZE);
    
    /* Adaptando a main do contexts.c
     * 
     * deve-se alocar um novo espaço na fila para esse contexto e atribuir
     * seu endereço para contexto->uc_stack e definir um sucessor para o
     * contexto e atribuir seu endereço para contexto->uc_link
     */
    
    if (stack)
    {
        task->context.uc_stack.ss_sp = stack ;
        task->context.uc_stack.ss_size = STACKSIZE;
        task->context.uc_stack.ss_flags = 0;
        task->context.uc_link = 0;
    }
    else
    {
        perror ("Erro na criação da pilha: ");
        
        return -1;
    }
    
    task->tid = id;//Sabendo que é diferente de 0
    task->init_time = systime();
    task->last_time = systime();
    task->activations = 0;
    makecontext (&(task->context),
                 (void*)(*start_routine),
                 1,
                 arg);
    
    
    /*
     * Aqui usando a queue que fizemos na P0
     */
    if (task->tid > 0)
    {
        /*Alan, estou usando a fila por que está enunciado no Maziero:
         * struct task_t *prev, *next ;   // para usar com a biblioteca de filas (cast)
         *
         * No entanto, pelo que que está no contexts.c a ucontext_t já possuí uma pilha implementada
         * pois bem, coloquei nesse if para cumprir o enunciado. Talvez tenha uma utilização mais a frente
         */
        
        
        task->prioDinamica = 0;//Inicializando prioridade dinamica em 0
        task->prioEstatica = 0;//Inicializando prioridade estatica em 0
        task->quantum = TIMEQUANTUM ;
        queue_append ((queue_t **) &taskQueue, (queue_t *) task);//Inserindo na fila        

        
    }
    
    #ifdef DEBUG
    printf ("\ntask_create: criou tarefa %d\n", task->tid);//Imprimindo a tarefa
    #endif  
    
    
    id++;//Incrementando o ID. Assim, não tem problemas de id com o mesmo número
    
    return taskAtual->tid;
}

void task_exit (int exitCode){
    
    
    #ifdef DEBUG
    printf("task_exit: tarefa %d sendo encerrada \n",taskAtual->tid);
    #endif
    
    
    
    /*Quando uma tarefa encerra, se ela nao for o dispatcher, devolve o controle do processador ao dispatcher. Se for o dispatcher, devolve o controle para a main*/
    
    if (taskAtual->tid !=  exitCode)
    {
	taskAtual->final_time = systime();
	printf("Task %d exit: running time %d ms, cpu time %d ms, %d activations \n", taskAtual->tid, taskAtual->final_time - taskAtual->init_time, taskAtual->proc_time, taskAtual->activations);
        
        task_switch(&dispatcher);
        
    }
    else
    {
	taskAtual->final_time = systime();
	printf("Task %d exit: running time %d ms, cpu time %d ms, %d activations \n", taskAtual->tid, taskAtual->final_time - taskAtual->init_time, taskAtual->proc_time, taskAtual->activations);
        task_switch(&task_main);
        
    }
}

int task_switch (task_t *task)
{
    /** Esta e a operação basica de troca de contexto, que encapsula a funcao swapcontext.
     *    Ela sera chamada sempre que for necessaria uma troca de contexto **/
    /* task: tarefa que ira assumir o processador
     *    retorno: valor negativo se houver erro, ou zero */
    
    task_t *aux;
    
    aux = taskAtual;
    taskAtual = task;
    if (taskAtual != task)
        return -1;
    
    #ifdef DEBUG
    printf ("task_switch: trocando contexto de %d para %d\n", aux->tid, task->tid) ;
    #endif
    taskAtual->last_time = systime();
    taskAtual->activations += 1;
    swapcontext( &(aux->context), &(task->context) );
    
    return 0;
}




int task_id ()
{
    /** retorno: Identificador numerico (ID) da tarefa corrente,
     *    que devera ser 0 para main, ou um valor positivo para as demais tarefas.
     *    Esse identificador e único: nao existem duas tarefas com o mesmo ID.**/
    
    return taskAtual->tid;
}


void task_setprio (task_t *task, int prio)
{
    
    /* Alan , implementei essa função foi desenvolvida de acordo com o enunciado do Maziero.
     * Link: http://wiki.inf.ufpr.br/maziero/doku.php?id=so:escalonador_por_prioridades
     *
     *
     * Esta função ajusta a prioridade estática da tarefa task para o valor prio
     * (que deve estar entre -20 e +20).
     * Caso task seja nulo, ajusta a prioridade da tarefa atual
     */
    
    if(task == NULL)
    {
        taskAtual->prioEstatica = prio;
        
        #ifdef DEBUG
        printf("task_setprio: Task %d prioridade -> %d\n",taskAtual->tid, prio);
        #endif
        
    }
    else if(prio>= -20 && prio <=20)
    {
        task->prioEstatica = prio;
        
        #ifdef DEBUG
        printf("task_setprio: Task %d prioridade -> %d\n",task->tid, prio);
        #endif
        
    }
    
}

void task_setprioDin (task_t *task, int prio)
{
    
    if(task == NULL)
    {
        taskAtual->prioDinamica = prio;
        
        #ifdef DEBUG
        printf("task_setprioDin: Task %d prioridade -> %d\n",taskAtual->tid, prio);
        #endif
        
    }
    else if(prio>= -20 && prio <=20)
    {
        task->prioDinamica = prio;
        
        #ifdef DEBUG
        printf("task_setprioDin: Task %d prioridade -> %d\n",task->tid, prio);
        #endif
        
    }
    
}

int task_getprio (task_t *task)
{
    if(task == NULL)
    {
        return taskAtual->prioEstatica;
    }
    else
        
        return task->prioEstatica;
}

int task_getprioDin (task_t *task)
{
    if(task == NULL)
    {
        return taskAtual->prioDinamica;
    }
    else
        
        return task->prioDinamica;
}



void tratador (int signum)
{
	time += 1;
	//printf ("Recebi o sinal %d\n", signum) ;
    
    /* Segundo o Maziero: http://wiki.inf.ufpr.br/maziero/doku.php?id=so:preempcao_por_tempo
     * 
     * O tratador do temporizador deve sempre verificar se a tarefa corrente é de usuário ou de sistema, 
     * antes de preemptá-la devido ao fim de um quantum. Pode ser adicionado um flag na estrutura de controle 
     * de cada tarefa para indicar se é uma tarefa de sistema ou de usuário.
     * 
     */
    
       
    #ifdef DEBUG
    printf("sigum: %d, quantum: %d, task_id: %d, time: %d ms \n", signum, quantum, taskAtual->tid, time);
    #endif
    
    if (taskAtual != &dispatcher) {
        if (taskAtual->quantum == 0)
            task_yield();
        else
            taskAtual->quantum--;
    }
    
 
    
}

unsigned int systime ()
{
	return time;
}

