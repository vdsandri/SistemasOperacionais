/*
 * Ping Pong OS
 * scheduler.c
 * Projeto da disciplina Sistemas Operacionais - UTFPR - 2017.02
 * Professor: Copetti
 * Alunos: Alan de Farias da Silva
 *         Vinícius Sandri Diaz
 *
 * Data da Ultima modificação: 18/10 by Vinícius
 *
 * pra compilar: gcc -o scheduler -W pingpong-scheduler.c ppos.c queue.c
 */


#include "ppos.h"

#define STACKSIZE 32768

task_t* taskAtual = NULL;
task_t* taskQueue = NULL;


task_t task_main;
task_t dispatcher;

int id = 0;



 task_t* scheduler()
{
	task_t* next;
	task_t* iterator;
    	int bestPrio = task_getprio(taskQueue) - task_getprioDin(taskQueue);
	iterator = taskQueue;
	next = iterator;
	do
        {
	    if ((task_getprio(iterator) - task_getprioDin(iterator)) < bestPrio)
	    {

            	bestPrio = (task_getprio(iterator) - task_getprioDin(iterator));
            	next = iterator;

	    }

            iterator = iterator->next;

        }while (iterator!= taskQueue);
	
	task_setprioDin(next, 0);
	iterator = taskQueue;
	do
        {
            task_setprioDin(iterator, task_getprioDin(iterator) + 1);
            iterator = iterator->next;

        }while (iterator!= taskQueue);

	return next;


}


//Essa função não existe na ppos.h
void dispatcher_body (void* arg)
{

    /* Alan essa função foi desenvolvida de acordo com a página do Maziero:
     * http://wiki.inf.ufpr.br/maziero/doku.php?id=so:dispatcher
     *
     * Praticamento é a mesma função descrita, só instanciei o ponteiro e
     * usei a queue para percorrer as tasks
     */
    task_t* next;
    next = NULL;

    // se o numero de tarefas for maior que zero
    while (queue_size ((queue_t *) taskQueue) > 0)
    {
        // ... // ações antes de lançar a tarefa "next", se houverem
        next = scheduler(); // scheduler é uma função

        //... // ações após retornar da tarefa "next", se houverem
        if(next)
        {
	    queue_remove((queue_t **) &taskQueue, (queue_t *) next);

            task_switch(next);// transfere controle para a tarefa "next"
	    //queue_append ((queue_t **) &taskQueue, (queue_t *) next); // Uma tarefa que ainda nao finalizou deve ser colocada novamente no final da fila
	

        }


    }

    task_exit(0); // encerra a tarefa dispatcher
}





void task_yield ()
{	
    /*Se a tarefa chamou yield, devolve o processador ao dispatcher e retorna ao fim da fila de tarefas prontas
*/
    if (taskAtual->tid > 0)
	queue_append ((queue_t **) &taskQueue, (queue_t *) taskAtual);
    task_switch(&dispatcher);
}

void ppos_init ()
{
    /*
     * Alan pensando no que conversamos ontem achei uma forma de
     * inicializar a task da main. Eu acho que do jeito que esta
     * não fica aquela coisa tipo do nada, sem contexto. E desta forma, a task_main é incializada
     * No entanto, não sei se vai ficar legal na pilha. Só foi uma idéia, se achar ela meio bosta
     * fique à vontade pra modificar
     *
     */

    taskQueue = NULL;
    task_main.tid = id; //Sendo que o id = 0

    /*
     * Inicializa a task de acordo com o site
     * https://www.systutorials.com/docs/linux/man/3-getcontext/
     * */

    getcontext(&task_main.context);
    taskAtual = &task_main;

    task_create(&dispatcher, dispatcher_body, "dispatcher");
    task_setprio(&dispatcher, -20);

    /*
     * desativa o buffer da saida padrao (stdout), usado pela função printf
     *
     * Essa função esta na página do mazeiro e ele recomeda desativar no inicio
     * por isso coloquei aqui. Qualquer coisa Alan, veja aqui:
     * http://wiki.inf.ufpr.br/maziero/doku.php?id=so:gestao_de_tarefas
     */
    setvbuf (stdout, 0, _IONBF, 0) ;

    #ifdef DEBUG
    printf ("task_init: inicializou a tarefa %d\n", taskAtual->tid) ;
    #endif
    //coloquei esse print de acordo com a recomedação do Mazieiro

    id++; //Incrementando o valor do id para outra task não receber a mesma id da task_main

}
int task_create (task_t * task, void (*start_routine)(void *),  void * arg)
{
    /*Alan, como vc pode ver no enunciado abaixo da página do Mazieiro,
     * adaptei a main da context.c nesta função. Vc pode conferir a dica abaixo
     */

    /* Atenção: deve ser previsto um descritor de tarefa que aponte para o programa principal
     *    ( que exercerá a mesma função da variável ContextMain no programa pingpong.c)
     */

    /* task: estrutura que referencia a tarefa criada
     *      start_routine: função que será executada pela tarefa
     *      arg: parâmetro a passar para a tarefa que está sendo criada
     *      retorno: o ID da task ou valor negativo, se houver erro
     */

    char* stack;
    task_t *aux;

    getcontext(&task->context);

    stack = malloc (STACKSIZE);

    /* Adaptando a main do contexts.c
     *
     * deve-se alocar um novo espaço na fila para esse contexto e atribuir
     * seu endereço para contexto->uc_stack e definir um sucessor para o
     * contexto e atribuir seu endereço para contexto->uc_link
     */

    if (stack)
    {
        task->context.uc_stack.ss_sp = stack ;
        task->context.uc_stack.ss_size = STACKSIZE;
        task->context.uc_stack.ss_flags = 0;
        task->context.uc_link = 0;
    }
    else
    {
        perror ("Erro na criação da pilha: ");

        return -1;
    }

    task->tid = id;//Sabendo que é diferente de 0

    makecontext (&(task->context),
                 (void*)(*start_routine),
                 1,
                 arg);


    /*
     * Aqui usando a queue que fizemos na P0
     */
    if (task->tid > 0)
    {
        /*Alan, estou usando a fila por que está enunciado no Maziero:
         * struct task_t *prev, *next ;   // para usar com a biblioteca de filas (cast)
         *
         * No entanto, pelo que que está no contexts.c a ucontext_t já possuí uma pilha implementada
         * pois bem, coloquei nesse if para cumprir o enunciado. Talvez tenha uma utilização mais a frente
         */

        queue_append ((queue_t **) &taskQueue, (queue_t *) task);//Inserindo na fila


        task->prioDinamica = 0;//Inicializando prioridade dinamica em 0
        task->prioEstatica = 0;//Inicializando prioridade estatica em 0

    }

    #ifdef DEBUG
    printf ("\ntask_create: criou tarefa %d\n", task->tid);//Imprimindo a tarefa
    queue_print ("Elementos da fila:  \n", (queue_t*) taskQueue, print_queue);//Imprimindo a tareda na fila
    #endif

    id++; //Incrementando o ID. Assim, não tem problemas de id com o mesmo número

    return taskAtual->tid;
}

void task_exit (int exitCode){


    #ifdef DEBUG
    printf("task_exit: tarefa %d sendo encerrada \n",taskAtual->tid);
    #endif



    /*Quando uma tarefa encerra, se ela nao for o dispatcher, devolve o controle do processador ao dispatcher. Se for o dispatcher, devolve o controle para a main*/

    if (taskAtual->tid !=  exitCode)
    {
        task_switch(&dispatcher);

    }
    else
    {
	task_switch(&task_main);
        
    }
}

int task_switch (task_t *task)
{
    /** Esta e a operação basica de troca de contexto, que encapsula a funcao swapcontext.
     *    Ela sera chamada sempre que for necessaria uma troca de contexto **/
    /* task: tarefa que ira assumir o processador
     *    retorno: valor negativo se houver erro, ou zero */

    task_t *aux;

    aux = taskAtual;
    taskAtual = task;

    if (taskAtual != task)
        return -1;

    #ifdef DEBUG
    printf ("task_switch: trocando contexto de %d para %d\n", aux->tid, task->tid) ;
    #endif

    swapcontext( &(aux->context), &(task->context) );

    return 0;
}




int task_id ()
{
    /** retorno: Identificador numerico (ID) da tarefa corrente,
     *    que devera ser 0 para main, ou um valor positivo para as demais tarefas.
     *    Esse identificador e único: nao existem duas tarefas com o mesmo ID.**/

    return taskAtual->tid;
}


void task_setprio (task_t *task, int prio)
{

     /* Alan , implementei essa função foi desenvolvida de acordo com o enunciado do Maziero.
     * Link: http://wiki.inf.ufpr.br/maziero/doku.php?id=so:escalonador_por_prioridades
     *
     *
     * Esta função ajusta a prioridade estática da tarefa task para o valor prio
     * (que deve estar entre -20 e +20).
     * Caso task seja nulo, ajusta a prioridade da tarefa atual
     */

    if(task == NULL)
    {
        taskAtual->prioEstatica = prio;

        #ifdef DEBUG
        printf("task_setprio: Task prioridade %d -> %d\n",taskAtual->tid, prio);
        #endif

    }
    else if(prio>= -20 && prio <=20)
    {
        task->prioEstatica = prio;

        #ifdef DEBUG
        printf("task_setprio: Task prioridade %d -> %d\n",task->tid, prio);
        #endif

    }

}

void task_setprioDin (task_t *task, int prio)
{

    if(task == NULL)
    {
        taskAtual->prioDinamica = prio;

        #ifdef DEBUG
        printf("task_setprio: Task prioridade %d -> %d\n",taskAtual->tid, prio);
        #endif

    }
    else if(prio>= -20 && prio <=20)
    {
        task->prioDinamica = prio;

        #ifdef DEBUG
        printf("task_setprio: Task prioridade %d -> %d\n",task->tid, prio);
        #endif

    }

}

int task_getprio (task_t *task)
{
    if(task == NULL)
    {
        return taskAtual->prioEstatica;
    }
    else

	return task->prioEstatica;
}

int task_getprioDin (task_t *task)
{
    if(task == NULL)
    {
        return taskAtual->prioDinamica;
    }
    else

	return task->prioDinamica;
}



